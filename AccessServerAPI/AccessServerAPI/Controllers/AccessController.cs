﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AccessServerAPI.Models;
using AccessServerAPI.Classes;
using Microsoft.AspNetCore.Mvc;

namespace AccessServerAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AccessController : ControllerBase
    {
        List<User> users = new List<User>()
        {
            new User() { Name = "Joe", ID = 0, AllowedBenches = new List<Bench>() { new Bench() { Number = 1 }, new Bench() { Number = 2 } } },
            new User() { Name = "Matt", ID = 1, AllowedBenches = new List<Bench>() },
            new User() { Name = "Seth", ID = 2, AllowedBenches = new List<Bench>() { new Bench() { Number = 1 } } },
            new User() { Name = "Daniel", ID = 4, AllowedBenches = new List<Bench>() { new Bench() { Number = 2 } } },
            new User() { Name = "John", ID = 3, AllowedBenches = new List<Bench>() { new Bench() { Number = 2 } } },
        };

        [HttpGet("{id}/{benchId}")]
        public bool Get(int id, int benchId)
        {
            var user = users.Where(u => u.ID == id).FirstOrDefault();
            if (user != null)
            {
                if (user.AllowedBenches.Any(b => b.Number == benchId))
                {
                    FileUtilities.Log(user.Name + " attemped to access bench " + benchId.ToString() + ". Access granted. " + DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss"));
                    return true;
                }
                else
                {
                    FileUtilities.Log(user.Name + " attemped to access bench " + benchId.ToString() + ". Access Denied." + DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss"));
                    return false;
                }
            }
            return false;
        }
    }
}