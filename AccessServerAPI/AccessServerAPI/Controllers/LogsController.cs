﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AccessServerAPI.Classes;
using Microsoft.AspNetCore.Mvc;

namespace AccessServerAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class LogsController : Controller
    {
        [HttpGet]
        public string Index()
        {
            return String.Join('\n', FileUtilities.GetLogs().ToArray());
        }
    }
}