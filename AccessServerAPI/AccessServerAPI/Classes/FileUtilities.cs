﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;

namespace AccessServerAPI.Classes
{
    public class FileUtilities
    {
        public static List<string> GetLogs()
        {
            List<string> readLogs = new List<string>();
            using (StreamReader sr = new StreamReader("C://logs/logs.txt"))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    readLogs.Add(line);
                }
            }
            return readLogs;
        }

        public static void Log(string log)
        {
            using (StreamWriter sw = File.AppendText("C://logs/logs.txt"))
            {
                sw.WriteLine(log);
            }
        }
    }
}
