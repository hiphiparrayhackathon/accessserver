﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AccessServerAPI.Classes;
using AccessServerAPI.Models;

namespace AccessServerAPI.Models
{
    public class User
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public List<Bench> AllowedBenches { get; set; }
    }
}
